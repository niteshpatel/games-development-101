/**
 * User: Nitesh
 * Date: 01/04/13
 * Time: 02:13
 */

function Match3() {
    var self = this;

    // Move all the lines downwards
    self.dropDown = function (grid, handler) {
        var width = grid.length;
        var height = grid[0].length;

        // Now shift all the rows above down one row
        var count;
        for (var i = 0; i < width; i++) {
            count = 0;
            for (var j = 0; j < height; j++) {

                // Count the number of positions the block drops
                if (grid[i][j] == null) {
                    count++;
                }

                // If the next block is occupied or the bottom of the grid
                // then we can finally perform the drop on all the relevant
                // blocks in the column
                if (count && ((j + 1 < height && grid[i][j + 1] != null) || j == height - 1)) {
                    for (var k = j; k - count >= 0; k--) {
                        grid[i][k] = grid[i][k - count];
                        grid[i][k - count] = null;

                        // Allow custom handling of the item after drop
                        if (handler) {
                            handler(grid[i][k], count);
                        }
                    }
                    break;
                }
            }
        }
        return grid;
    };

    // Remove the chain from the grid, given another grid (effectively
    // performs a boolean AND operation)
    self.removeChain = function (grid, chain, handler) {
        for (var i = 0; i < grid.length; i++) {
            for (var j = 0; j < grid[i].length; j++) {
                if (chain[i][j]) {

                    // Allow custom handling of the item after removal
                    if (handler) {
                        handler(grid[i][j]);
                    }
                    grid[i][j] = null;
                }
            }
        }
        return grid;
    };

    // Check for a chain of connected gems starting at x, y; return the
    // position of the chain as an boolean array
    self.getChain = function (grid, x, y, key, checked) {
        var width = grid.length;
        var height = grid[0].length;

        // Initialise the array if necessary
        if (!checked) {
            checked = [];
            for (var i = 0; i < width; i++) {
                checked.push([]);
                for (var j = 0; j < height; j++) {
                    checked[i].push(0);
                }
            }
        }

        if (!checked[x][y] && grid[x][y]) {

            if (y + 1 < height && key(grid[x][y]) == key(grid[x][y + 1])) {
                checked[x][y] = 1;
                self.getChain(grid, x, y + 1, key, checked);
            }
            if (x + 1 < width && key(grid[x][y]) == key(grid[x + 1][y])) {
                checked[x][y] = 1;
                self.getChain(grid, x + 1, y, key, checked);
            }
            if (y - 1 > -1 && key(grid[x][y]) == key(grid[x][y - 1])) {
                checked[x][y] = 1;
                self.getChain(grid, x, y - 1, key, checked);
            }
            if (x - 1 > -1 && key(grid[x][y]) == key(grid[x - 1][y])) {
                checked[x][y] = 1;
                self.getChain(grid, x - 1, y, key, checked);
            }
        }
        return checked;
    };

    // Counts the number of non-zero elements in any array passed in
    self.getCount = function (grid) {
        var width = grid.length;
        var height = grid[0].length;

        var counter = 0;
        for (var i = 0; i < width; i++) {
            for (var j = 0; j < height; j++) {
                if (grid[i][j]) {
                    counter++;
                }
            }
        }
        return counter;
    }
}