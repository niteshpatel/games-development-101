/**
 * Created by Nitesh on 14/12/2014.
 */

function AnimateGemRemove(screen, gem) {
    var self = this;

    self.getZIndex = function () {
        return -180;
    };

    self.onCreate = function (screen, gem) {
        self.screen = screen;
        self.gem = gem;

        // Initialise timer
        self.timer = 0;

        // Set the image config
        self.angle = 0;
        self.scaleDown = 1;
    };

    self.isDone = function () {
        return self.timer > 1000;
    };

    self.update = function () {
        self.timer += self.screen.parent.interval;
        self.angle += 20;
        self.scaleDown /= 1.125;
    };

    self.paint = function (c) {
        c.save();
        c.translate(self.gem.posX + 25, self.gem.posY + 25);
        c.scale(self.scaleDown, self.scaleDown);
        c.rotate(self.angle * Math.PI / 180);
        c.drawImage(self.gem.image, -25, -25);
        c.restore();
    };

    self.onCreate(screen, gem);
}


