/**
 * Created by Nitesh on 14/12/2014.
 */

function AnimateGemDrop(gem, count, speed) {
    var self = this;

    self.onCreate = function (gem, count, speed) {
        self.gem = gem;

        // Initialise counter
        self.count = count * 50;
        self.speed = speed;
    };

    self.isDone = function () {
        return self.count <= 0;
    };

    self.update = function () {
        var change = Math.min(self.speed, self.count);
        self.count -= change;
        self.gem.posY += change;
    };

    self.onCreate(gem, count, speed);
}
