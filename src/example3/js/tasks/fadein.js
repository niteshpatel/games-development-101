/**
 * Created by Nitesh on 14/12/2014.
 */

function FadeIn(screen, colour, duration) {
    var self = this;

    self.onCreate = function (screen, colour, duration) {
        self.colour = colour;
        self.duration = duration;
        self.alpha = 1;

        // The fade increment
        self.increment = screen.parent.interval / duration;
    };

    self.isDone = function () {
        return self.alpha <= 0;
    };

    self.update = function () {
        self.alpha -= self.increment;
        self.alpha = Math.max(self.alpha, 0);
    };

    self.paint = function (c) {
        c.save();
        c.globalAlpha = self.alpha;
        c.fillStyle = self.colour;
        c.rect(0, 0, c.canvas.width, c.canvas.height);
        c.fill();
        c.restore();
    };

    self.onCreate(screen, colour, duration);
}

