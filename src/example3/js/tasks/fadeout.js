/**
 * Created by Nitesh on 14/12/2014.
 */

function FadeOut(screen, colour, duration) {
    var self = this;

    self.onCreate = function (screen, colour, duration) {
        self.colour = colour;
        self.duration = duration;
        self.alpha = 0;

        // The fade increment
        self.increment = screen.parent.interval / duration;
        self.finished = false;
    };

    self.isDone = function () {
        return self.finished;
    };

    self.onPause = function () {
        self.finished = true;
    };

    self.update = function () {
        if (self.alpha < 1) {
            self.alpha += self.increment;
            self.alpha = Math.min(self.alpha, 1);
        }
    };

    self.paint = function (c) {
        c.save();
        c.globalAlpha = self.alpha;
        c.fillStyle = self.colour;
        c.rect(0, 0, c.canvas.width, c.canvas.height);
        c.fill();
        c.restore();
    };

    self.onCreate(screen, colour, duration);
}
