/**
 * Created by Nitesh on 14/12/2014.
 */

function ScreenSplash(parent) {
    var self = this;

    self.onCreate = function (parent) {
        self.screenId = "splash";
        self.parent = parent;

        // Background pre-load
        self.bg = new Image();
        self.bg.src = "images/splash.png";

        // Timer for splash
        self.timer = null;
    };

    self.onResume = function () {
        // Set timer for splash
        self.timer = 3000;

        // Fade in screen
        self.subtasks.add(new FadeIn(self, "black", 200));
    };

    self.update = function () {
        self.timer -= self.parent.interval;
        if (self.timer <= 0) {
            self.subtasks.add(new FadeOut(self, "black", 200));
            window.setTimeout(function () {
                self.parent.changeScreen("game");
            }, 200);
        }
    };

    self.paint = function () {
        self.painter.add(function (c) {
            c.drawImage(self.bg, 0, 0, 360, 640);
        }, 0);
    };

    self.onCreate(parent);
}
