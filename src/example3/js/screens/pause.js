/**
 * Created by Nitesh on 14/12/2014.
 */

function ScreenPause(parent) {
    var self = this;

    self.onCreate = function (parent) {
        self.screenId = "pause";
        self.parent = parent;

        // Background pre-load
        self.bg = new Image();
        self.bg.src = "images/pause.png";
    };

    self.onResume = function () {
        // Fade in screen
        self.subtasks.add(new FadeIn(self, "black", 400));
    };

    self.onInput = function () {
        // Handle presses
        if (self.parent.mouseDown.pageX) {
            var posX = self.parent.mouseDown.pageX;
            var posY = self.parent.mouseDown.pageY;

            // Clear input buffer
            self.parent.mouseDown = {};

            // Check for continue button
            if (posX >= 55 && posY >= 245 && posX < 300 && posY < 290) {
                self.subtasks.add(new FadeOut(self, "black", 200));
                window.setTimeout(function () {
                    self.parent.stopScreen();
                    self.parent.resumeScreen("game");
                }, 200);
            }

            // Check for restart button
            if (posX >= 55 && posY >= 315 && posX < 300 && posY < 360) {
                self.subtasks.add(new FadeOut(self, "black", 200));
                window.setTimeout(function () {
                    self.parent.changeScreen("game");
                }, 200);
            }

            // Check for quit button
            if (posX >= 55 && posY >= 385 && posX < 300 && posY < 430) {
                self.subtasks.add(new FadeOut(self, "black", 200));
                window.setTimeout(function () {
                    self.parent.changeScreen("splash");
                }, 200);
            }
        }
    };

    self.update = function () {
    };

    self.paint = function () {
        self.painter.add(function (c) {
            c.drawImage(self.bg, 0, 0, 360, 640);
        }, 0);
    };

    self.onCreate(parent);
}