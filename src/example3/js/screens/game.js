/**
 * Created by Nitesh on 14/12/2014.
 */

function ScreenGame(parent) {
    var self = this;

    self.onCreate = function (parent) {
        self.screenId = "game";
        self.parent = parent;

        // Background pre-load
        self.bg = new Image();
        self.bg.src = "images/bg_game.png";
        self.bg_ex1 = new Image();
        self.bg_ex1.src = "images/bg_game_ex1.png";
        self.timerbar = new Image();
        self.timerbar.src = "images/timerbar.png";

        // Array to store gems
        self.gems = null;

        // Score and timer
        self.score = null;
        self.timer = null;
        self.timer_stopped = null;

        // Get a match 3 helper
        self.match3 = new Match3();
    };

    self.onResume = function () {
        // Fade in screen
        self.subtasks.add(new FadeIn(self, "black", 200));
    };

    self.onStart = function () {
        // Initialise gems
        self.gems = [];
        for (var i = 0; i < 7; i++) {
            self.gems[i] = [];
            for (var j = 0; j < 8; j++) {
                var gemId = Math.floor(Math.random() * 5);
                self.gems[i].push(new Gem(gemId, i, j));
            }
        }

        // Set up timer and score
        self.score = 0;
        self.timer = 60000;
        self.timer_stopped = false;
    };

    self.replaceGems = function () {
        // Replace missing gems
        for (var i = 0; i < 7; i++) {
            for (var j = 0; j < 8; j++) {
                if (self.gems[i][j] == null) {
                    var gemId = Math.floor(Math.random() * 5);

                    self.gems[i][j] = new Gem(gemId, i, j - 5);
                    self.subtasks.add(new AnimateGemDrop(self.gems[i][j], 5, 40));
                }
            }
        }
    };

    self.onInput = function () {
        // Handle presses
        if (self.parent.mouseDown.pageX) {
            var posX = self.parent.mouseDown.pageX;
            var posY = self.parent.mouseDown.pageY;

            // Clear input buffer
            self.parent.mouseDown = {};

            // Check for gem presses
            if (posX >= 5 && posY >= 130 && posX < 355 && posY < 530) {

                // Calculate gem positions
                var gemX = Math.floor((posX - 5) / 50);
                var gemY = Math.floor((posY - 130) / 50);

                // Check for a chain
                var key = function (item) {
                    return item.gemId
                };
                var chain = self.match3.getChain(self.gems, gemX, gemY, key);
                var count = self.match3.getCount(chain);

                // Only remove gems if there was chain larger than 2
                if (count > 1) {

                    // Now remove the games
                    self.match3.removeChain(self.gems, chain, function (gem) {
                        self.subtasks.add(new AnimateGemRemove(self, gem));
                    });
                    self.match3.dropDown(self.gems, function (gem, count) {
                        self.subtasks.add(new AnimateGemDrop(gem, count, 30));
                    });

                    // Update the score
                    self.score += 360 * count - 800;
                }

                // Replace removed gems
                self.replaceGems();
            }

            // Check for pause button
            if (posX >= 310 && posY >= 60 && posX < 350 && posY < 100) {
                self.subtasks.add(new FadeOut(self, "black", 200));
                window.setTimeout(function () {
                    self.parent.pauseScreen();
                    self.parent.resumeScreen("pause");
                }, 200);
            }
        }
    };

    self.update = function () {
        // Update timer
        self.timer -= self.parent.interval;

        // If the timer is 0, end the game
        if (self.timer < 0 && !self.timer_stopped) {
            self.timer_stopped = true;
            self.parent.changeScreen("splash");
        }
    };

    self.paint = function () {
        // Paint background
        self.painter.add(function (c) {
            c.drawImage(self.bg, 0, 0, 360, 640);
        }, -200);

        // Paint gems
        self.painter.add(function (c) {
            var gem;
            for (var i = 0; i < 7; i++) {
                for (var j = 0; j < 8; j++) {
                    gem = self.gems[i][j];
                    if (gem) {
                        gem.paint(c, i, j);
                    }
                }
            }
        }, -150);

        // Paint foreground
        self.painter.add(function (c) {
            c.drawImage(self.bg_ex1, 0, 36, 360, 89);
        }, -120);

        // Paint score
        self.painter.add(function (c) {
            c.font = "16px Comic Sans MS";
            c.fillStyle = "#FFFFFF";
            c.strokeStyle = "#000000";
            var text = self.score.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            var size = c.measureText(text).width / 2;
            c.strokeText(text, 243 - size, 86);
            c.fillText(text, 243 - size, 86);
        }, -110);


        // Paint timer
        self.painter.add(function (c) {
            c.font = "22px Comic Sans MS";
            c.fillStyle = "#FFFFFF";
            c.strokeStyle = "#000000";
            var time = Math.ceil(self.timer / 1000);
            var text = time.toString();
            var size = c.measureText(text).width / 2;
            c.drawImage(self.timerbar, (60 - time) * -6, 551, 365, 35);
            c.strokeText(text, 180 - size, 577);
            c.fillText(text, 180 - size, 577);
        }, -105);
    };

    self.onCreate(parent);
}

function Gem(gemId, posX, posY) {
    var self = this;

    var getImageById = function (gemId) {
        return {
            0: "images/gem_blue.png",
            1: "images/gem_green.png",
            2: "images/gem_purple.png",
            3: "images/gem_red.png",
            4: "images/gem_yellow.png"
        }[gemId];
    };

    self.onCreate = function (gemId, posX, posY) {
        self.gemId = gemId;
        self.posX = (posX * 50) + 5;
        self.posY = (posY * 50) + 130;

        // Set the gem image
        self.image = new Image();
        self.image.src = getImageById(gemId);
    };

    self.paint = function (c) {
        c.drawImage(self.image, self.posX, self.posY);
    };

    self.onCreate(gemId, posX, posY);
}
