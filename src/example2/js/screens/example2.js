/**
 * User: Nitesh
 * Date: 31/03/13
 * Time: 15:02
 */

function ScreenExample2(parent) {
    var self = this;

    self.onCreate = function (parent) {
        self.screenId = "example2";
        self.parent = parent;
    };

    self.onStart = function () {
        self.bat1Pos = {x: 0, y: 0};
        self.bat2Pos = {x: 400, y: 0};
        self.ballPos = {x: 80, y: 40};
        self.ballSpeed = {x: 8, y: 5};
        self.ballColor = 0;
    };

    self.onInput = function () {
        // Handle presses
        if (self.parent.keyDown) {
            var key = self.parent.keyDown.key;

            // Clear input buffer
            self.parent.keyDown = {};

            // Up
            if (key == 38) {
                self.bat1Pos.y -= 20;
                if (self.bat1Pos.y < 0) {
                    self.bat1Pos.y = 0;
                }
            }
            // Down
            else if (key == 40) {
                self.bat1Pos.y += 20;
                if (self.bat1Pos.y > 340) {
                    self.bat1Pos.y = 340;
                }
            }
        }
    };

    self.update = function () {
        // Move ball
        self.ballPos.x += self.ballSpeed.x;
        self.ballPos.y += self.ballSpeed.y;

        // Check contact with bat
        if (self.ballSpeed.x < 0 &&
            self.ballPos.x <= self.bat1Pos.x + 20 &&
            self.ballPos.y >= self.bat1Pos.y - 20 &&
            self.ballPos.y <= self.bat1Pos.y + 80) {
            self.ballSpeed.x = -self.ballSpeed.x;

            // Change Y based on contact point
            self.ballSpeed.y = (self.ballPos.y - self.bat1Pos.y - 30) / 6;
        }

        // Bounce ball off walls
        if (self.ballPos.x < 10 && self.ballSpeed.x < 0) {
            self.ballPos.x = 0;
            self.ballSpeed.x = -self.ballSpeed.x;
        }
        if (self.ballPos.y < 10 && self.ballSpeed.y < 0) {
            self.ballPos.y = 0;
            self.ballSpeed.y = -self.ballSpeed.y;
        }
        if (self.ballPos.x > 380 && self.ballSpeed.x > 0) {
            self.ballPos.x = 380;
            self.ballSpeed.x = -self.ballSpeed.x;
        }
        if (self.ballPos.y > 400 && self.ballSpeed.y > 0) {
            self.ballPos.y = 400;
            self.ballSpeed.y = -self.ballSpeed.y;
        }

        // Bat 2 just follows ball
        if (self.ballPos.x < 200 || self.ballSpeed.x < 0) {
            if (self.bat2Pos.y > 160) {
                self.bat2Pos.y -= 3;
            }
            if (self.bat2Pos.y < 160) {
                self.bat2Pos.y += 3;
            }
        }
        else {
            if (self.ballPos.y > self.bat2Pos.y + 50) {
                self.bat2Pos.y += 7;
            }
            else if (self.ballPos.y < self.bat2Pos.y + 30) {
                self.bat2Pos.y -= 7;
            }
        }

        // Limit bat within bounds
        if (self.bat2Pos.y >= 340) {
            self.bat2Pos.y = 340;
        }
        else if (self.bat2Pos.y <= 0) {
            self.bat2Pos.y = 0;
        }
    };

    self.paint = function () {
        // Clear screen
        self.painter.add(function (c) {
            c.clearRect(0, 0, c.canvas.width, c.canvas.height);
        }, 0);

        // Draw border
        self.painter.add(function (c) {
            c.fillStyle = "#000000";
            c.fillRect(0, 0, 420, 420);
        }, 10);

        // Paint bat
        self.painter.add(function (c) {
            c.fillStyle = "#0000FF";
            c.fillRect(self.bat1Pos.x, self.bat1Pos.y, 20, 80);
        }, 15);

        // Paint bat2
        self.painter.add(function (c) {
            c.fillStyle = "#00FF00";
            c.fillRect(self.bat2Pos.x, self.bat2Pos.y, 20, 80);
        }, 15);

        // Paint ball
        self.painter.add(function (c) {
            self.ballColor += 1;
            self.ballColor = self.ballColor % 22;
            var letters = '56789ABCDEFFEDCBA98765'.split('');
            var r = letters[self.ballColor];
            c.beginPath();
            c.arc(self.ballPos.x + 10, self.ballPos.y + 10, 8, 0, 2 * Math.PI);
            c.fillStyle = "#FF" + r + r + r + r;
            c.fill();
        }, 20);
    };

    self.onCreate(parent);
}
