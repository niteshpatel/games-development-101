/**
 * User: Nitesh
 * Date: 31/03/13
 * Time: 15:02
 */

function ScreenExample1(parent) {
    var self = this;

    self.onCreate = function (parent) {
        self.screenId = "example1";
        self.parent = parent;
    };

    self.onStart = function () {
        self.bat1Pos = {x: 0, y: 0};
        self.ballPos = {x: 80, y: 40};
    };

    self.onInput = function () {
        // Handle presses
        if (self.parent.keyDown) {
            var key = self.parent.keyDown.key;

            // Clear input buffer
            self.parent.keyDown = {};

            // Up
            if (key == 38) {
                self.bat1Pos.y -= 20;
                if (self.bat1Pos.y < 0) {
                    self.bat1Pos.y = 400;
                }
                if (self.bat1Pos.x == self.ballPos.x && self.bat1Pos.y == self.ballPos.y) {
                    self.ballPos.y -= 80;
                    if (self.ballPos.y < 0) {
                        self.ballPos.y = 400;
                    }
                }
            }
            // Down
            else if (key == 40) {
                self.bat1Pos.y += 20;
                if (self.bat1Pos.y > 400) {
                    self.bat1Pos.y = 0;
                }
                if (self.bat1Pos.x == self.ballPos.x && self.bat1Pos.y == self.ballPos.y) {
                    self.ballPos.y += 80;
                    if (self.ballPos.y > 400) {
                        self.ballPos.y = 0;
                    }
                }
            }
            // Left
            else if (key == 37) {
                self.bat1Pos.x -= 20;
                if (self.bat1Pos.x < 0) {
                    self.bat1Pos.x = 400;
                }
                if (self.bat1Pos.x == self.ballPos.x && self.bat1Pos.y == self.ballPos.y) {
                    self.ballPos.x -= 80;
                    if (self.ballPos.x < 0) {
                        self.ballPos.x = 400;
                    }
                }
            }
            // Right
            else if (key == 39) {
                self.bat1Pos.x += 20;
                if (self.bat1Pos.x > 400) {
                    self.bat1Pos.x = 0;
                }
                if (self.bat1Pos.x == self.ballPos.x && self.bat1Pos.y == self.ballPos.y) {
                    self.ballPos.x += 80;
                    if (self.ballPos.x > 400) {
                        self.ballPos.x = 0;
                    }
                }
            }
        }
    };

    self.update = function () {
    };

    self.paint = function () {
        // Clear screen
        self.painter.add(function (c) {
            c.clearRect(0, 0, c.canvas.width, c.canvas.height);
        }, 0);

        // Draw border
        self.painter.add(function (c) {
            c.fillStyle = "#DDDDDD";
            c.fillRect(0, 0, 420, 420);
        }, 10);

        // Paint bat
        self.painter.add(function (c) {
            c.fillStyle = "#0000FF";
            c.strokeWidth = "5px";
            c.fillRect(self.bat1Pos.x, self.bat1Pos.y, 20, 20);
        }, 15);

        // Paint ball
        self.painter.add(function (c) {
            c.beginPath();
            c.arc(self.ballPos.x + 10, self.ballPos.y + 10, 8, 0, 2 * Math.PI);
            c.fillStyle = "#FF0000";
            c.fill();
        }, 20);
    };

    self.onCreate(parent);
}
